var express = require('express');
var Location = require('./Location');


exports.GetLocation = function GetLocation(address, street, city, postcode) {
    try {
        return Location.find({address : address,
            street  : street,
            city    : city,
            postcode: postcode}
        )
    } catch (error) {
        throw new Error(`Unable to find location with address "${address}".`)
    }
}

exports.GetLocationByPostCode = function (postcode) {
    try {
        return Location.find({postcode: postcode}
        )
    } catch (error) {
        throw new Error(`Unable to find location with postcode "${postcode}".`)
    }
}

exports.GetLocationBySuburb = function (suburb) {
    try {
        return Location.find({city: suburb}
        )
    } catch (error) {
        throw new Error(`Unable to find location with suburb "${suburb}".`)
    }
}

exports.GetLocationByStreet = function (street, postcode) {
    try {
        return Location.find({street  : street, postcode: postcode}
        )
    } catch (error) {
        throw new Error(`Unable to find location with address "${address}".`)
    }
}

exports.InsertLocation = async function (address, street, city, postcode) {
    try {
        return await Location.findOneAndUpdate({
            'postcode': postcode,
            'street': street,
            'city': city,
            'address': address
        },
            { $set: { 'postcode': postcode, 'street': street, 'city': city, 'address': address } },
            { upsert: true, new: true }
        );
    } catch (error) {
        throw new Error(`Unable to find and update location with address "${address}".`)
    }
};




