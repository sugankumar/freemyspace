var mongoose = require('mongoose');  
const AutoIncrement = require('mongoose-sequence')(mongoose);

var LocationSchema = new mongoose.Schema({
  address: String,
  street: String,
  city: String,
  postcode: String,
  lastupdated:  {type: Date, default: Date.now}
});
mongoose.model('Location', LocationSchema);

LocationSchema.plugin(AutoIncrement, {inc_field: 'id'});

module.exports = mongoose.model('Location');