var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Location = require('../location/Location');

// CREATES A NEW Location
router.post('/', function (req, res) {
    Location.create({
            address : req.body.address,
            street  : req.body.street,
            city    : req.body.city,
            postcode : req.body.postcode   
        }, 
        function (err, location) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(location);
        });
});

// RETURNS ALL THE Locations IN THE DATABASE
router.get('/', function (req, res) {
    Location.find({}, function (err, locations) {
        if (err) return res.status(500).send("There was a problem finding the locations.");
        res.status(200).send(locations);
    });
});

// GETS A SINGLE Location FROM THE DATABASE
router.get('/:id', function (req, res) {
    Location.findById(req.params.id, function (err, location) {
        if (err) return res.status(500).send("There was a problem finding the location.");
        if (!location) return res.status(404).send("No location found.");
        res.status(200).send(location);
    });
});

// GETS all Locations FROM THE DATABASE BY city
router.get('/city/:city', function (req, res) {
    Location.find({'city':{ $regex : new RegExp(req.params.city, "i") }}, function (err, location) {
        if (err) return res.status(500).send("There was a problem finding the location.");
        if (!location) return res.status(404).send("No location found.");
        res.status(200).send(location);
    });
});

// GETS all Locations FROM THE DATABASE BY postcode
router.get('/postcode/:postcode', function (req, res) {
    Location.find({'postcode':req.params.postcode}, function (err, location) {
        if (err) return res.status(500).send("There was a problem finding the location.");
        if (!location) return res.status(404).send("No location found.");
        res.status(200).send(location);
    });
});

// GETS all Locations FROM THE DATABASE BY Street and postcode
///locations/street/bodramplace/3000
router.get('/street/:street/:postcode', function (req, res) {
    Location.find({'postcode':req.params.postcode, 'street':req.params.street}, function (err, location) {
        if (err) return res.status(500).send("There was a problem finding the location based on street.");
        if (!location) return res.status(404).send("No location found.");
        res.status(200).send(location);
    });
});

// DELETES A Location FROM THE DATABASE
router.delete('/:id', function (req, res) {
    Location.findByIdAndRemove(req.params.id, function (err, location) {
        if (err) return res.status(500).send("There was a problem deleting the location.");
        res.status(200).send("Location ID: "+ location.id +" was deleted.");
    });
});

// UPDATES A SINGLE Location IN THE DATABASE
router.put('/:id', function (req, res) {
    Location.findByIdAndUpdate(req.params.id, req.body, {new: true, upsert:true, setDefaultsOnUpdate: true }, function (err, location) {
        if (err) return res.status(500).send("There was a problem updating the location.");
        res.status(200).send(location);
    });
});


module.exports = router;