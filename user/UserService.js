var express = require('express');
var User = require('./User');

exports.GetUser = function (email)
{
    try {
        return User.findOne({email : email});
    } catch (error) {
        throw new Error(`Unable to find user with email "${email}".`)
    }
};

exports.GetUserById = function (userid)
{
    try {
        return User.findOne({'userid':userid});
    } catch (error) {
        throw new Error(`Unable to find user with userid "${userid}".`)
    }
};
