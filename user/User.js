var mongoose = require('mongoose');  
const AutoIncrement = require('mongoose-sequence')(mongoose);

var UserSchema = new mongoose.Schema({  
  userid: Number,
  username: String,
  email: String,
  password: String,
  familyname: String,
  givenname: String,
  lastupdated:  {type: Date, default: Date.now}
});

mongoose.model('User', UserSchema);

UserSchema.plugin(AutoIncrement, {inc_field: 'userid'});

module.exports = mongoose.model('User');