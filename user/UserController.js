var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var fs = require('fs');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var User = require('./User');

// CREATES A NEW USER
router.post('/', function (req, res) {
    User.create({
            username : req.body.username,
            email    : req.body.email,
            password : req.body.password,
            familyname : req.body.familyname,
            givenname  : req.body.givenname            
        }, 
        function (err, user) {
            if (err) return res.status(500).send("There was a problem adding the information to the database.");
            res.status(200).send(user);
        });
});

// RETURNS ALL THE USERS IN THE DATABASE
router.get('/', function (req, res) {
    User.find({}, function (err, users) {
        if (err) return res.status(500).send("There was a problem finding the users.");
        res.status(200).send(users);
    });
});

// GETS A SINGLE USER FROM THE DATABASE
router.get('/:userid', function (req, res) {
    User.findOne({'userid':req.params.userid}, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});


// GETS A SINGLE USER FROM THE DATABASE BY NAME
router.get('/name/:name', function (req, res) {
    User.findOne({'name':req.params.name}, function (err, user) {
        if (err) return res.status(500).send("There was a problem finding the user.");
        if (!user) return res.status(404).send("No user found.");
        res.status(200).send(user);
    });
});

// DELETES A USER FROM THE DATABASE
router.delete('/:id', function (req, res) {
    User.findOneAndRemove({'id':req.params.id}, function (err, user) {
        if (err) return res.status(500).send("There was a problem deleting the user.");
        res.status(200).send("User: "+ user.familyname +" "+ user.givenname +" was deleted.");
    });
});

// UPDATES A SINGLE USER IN THE DATABASE
router.put('/:userid', function (req, res) {
    User.findOneAndUpdate({'userid':req.params.userid}, req.body, {new: true, upsert:true, setDefaultsOnUpdate: true }, function (err, user) {
        if (err) return res.status(500).send("There was a problem updating the user.");
        res.status(200).send(user);
    });
});


module.exports = router;