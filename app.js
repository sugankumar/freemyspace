var express = require('express');
var app = express();
const multer = require('multer')
const bodyParser = require('body-parser')

var db = require('./db');

var UserController = require('./user/UserController');
app.use('/users', UserController);

var LocationController = require('./location/LocationController');
app.use('/locations', LocationController);

var ItemController = require('./item/ItemController');
app.use('/items', ItemController);

app.use('/images', express.static(__dirname + '/Images'));

//Upload image
app.use(bodyParser.json());

const Storage = multer.diskStorage({
    destination(req, file, callback) {
      callback(null, './images')
    },
    filename(req, file, callback) {
      //callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`)
      callback(null, `${file.originalname}`)
    },
  })
  
  const upload = multer({ storage: Storage })
  
  
  app.post('/images/upload', upload.array('image', 3), (req, res) => {
    console.log('file', req.files)
    console.log('body', req.body.anybody)
    res.status(200).json({
      message: req.files[0].destination + '/'+ req.files[0].filename,
    })
  })


module.exports = app;