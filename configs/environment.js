var _Environments = {
    production:  {BASE_URL: '', API_KEY: ''},
    staging:     {BASE_URL: '', API_KEY: ''},
    development: {IMAGE_DIR: 'C:/MyThing/freemyspace-API/images/', 
                  DB_URL   : 'mongodb://localhost:27017/freemyspace'},
}

function getEnvironment() {
    // Insert logic here to get the current platform (e.g. staging, production, etc)
    var platform = getPlatform()
    return _Environments[platform];    
}

function getPlatform() {
    return 'development';
}

var Environment = getEnvironment()
module.exports = Environment