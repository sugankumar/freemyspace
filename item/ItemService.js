var express = require('express');
var Item = require('./Item');
var LocationService = require('../location/LocationService')
var UserService = require('../user/UserService')
var ItemImage = require('./ItemImage')


exports.GetItemsByName = function (name, callback) {
    try {
        Item.find({'name': { $regex: '.*' + name + '.*' }  },function(err,items){
            callback(items);
        });
       
    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with name "${name}".`)
    }
}

exports.GetItemsByID = function (id, callback) {
    try {
        Item.find({ _id: id }).populate("itemImage").exec(function (err, item) {
            //console.log(item);
            callback(item);
        });
    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find item with id "${id}".`)
    }
}

exports.GetItemsByPostcode = function (postcode, callback) {
    try {
        (async () => {
            var locations = await LocationService.GetLocationByPostCode(postcode);
            Item.find({ location: { $in: locations }}, function (err, items) {
                callback(items);
            }).populate("itemImage");
        })()

    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with name "${postcode}".`)
    }
}


exports.GetItemsBySuburb = function (suburb, callback) {
    try {
        (async () => {
            var locations = await LocationService.GetLocationBySuburb(suburb);
            Item.find({ location: { $in: locations }}, function (err, items) {
                callback(items);
            }).populate("itemImage");
        })()

    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with name "${postcode}".`)
    }
}

//Get Items for either suburb or Name
exports.GetItemsBySuburbOrName = function (any, callback) {
    try {
        (async () => {
            Item.find({ });
            var locations = await LocationService.GetLocationBySuburb(any);
            Item.find( { $or:[ {'location':{ $in: locations }}, {'name':{ $regex: new RegExp(any, "i") }} ]}, 
              function (err, items) {
                callback(items);
            }).populate("itemImage");
        })()

    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with name "${postcode}".`)
    }
}

exports.GetItemsByStreet = function (street, postcode, callback) {
    try {
        (async () => {
            var locations = await LocationService.GetLocationByStreet(street,postcode);
            console.log(locations);

            Item.find({ location: { $in: locations }}, function (err, items) {
                callback(items);
            });
        })()

    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with street "${stret}".`)
    }
}


exports.GetItemsByUser = function (userid, callback) {
    try {
        (async () => {
            Item.find({ userid: userid}).populate("itemImage").populate("location").exec(function (err, items) {
                callback(items);
            });
        })()
    } catch (error) {
        console.log(error);
        throw new Error(`Unable to find items with user "${userid}".`)
    }
}


exports.UpdateItemImage = async function (description, imagePath) {
    try {
        return await ItemImage.findOneAndUpdate({
            'description': description,
            'imagePath': imagePath
        },
            { $set: { 'description': description, 'imagePath': imagePath} },
            { upsert: true, new: true }
        );
    } catch (error) {
        throw new Error(`Unable to insert image "${description}".`)
    }
};


