var mongoose = require('mongoose');  
const AutoIncrement = require('mongoose-sequence')(mongoose);

var ItemImageSchema = new mongoose.Schema({
  itemImageid: Number,
  description: String,
  imagePath    : String,
  content      : String,
  updated:  {type: Date, default: Date.now}
});
mongoose.model('ItemImage', ItemImageSchema);

ItemImageSchema.plugin(AutoIncrement, {inc_field: 'itemImageid'});

module.exports = mongoose.model('ItemImage');