var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');
var Location = require('../location/Location');
var ItemImage = require('./ItemImage');
var Environment = require('../configs/environment');

var connection = mongoose.createConnection(Environment.DB_URL, { useNewUrlParser: true }); 
autoIncrement.initialize(connection);

var ItemSchema = new mongoose.Schema({
  itemid        : Number,
  location      : {type: mongoose.Schema.Types.ObjectId, ref: 'Location'},
  description   : String,
  name          : String,
  type          : String,
  dateAdded     : Date,
  isAvailable   : Boolean,
  dateAvailable : Date,
  itemImage     : [{type: mongoose.Schema.Types.ObjectId, ref: 'ItemImage'}],
  updated       : {type: Date, default: Date.now},
  userid        : Number
});

ItemSchema.plugin(autoIncrement.plugin, 'Item');

mongoose.model('Item', ItemSchema);

//ItemSchema.plugin(AutoIncrement, {inc_field: 'itemid'});

module.exports = mongoose.model('Item');