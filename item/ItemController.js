var express = require('express');
var router = express.Router();
const multer = require('multer');
const bodyParser = require('body-parser');
var fs = require('fs');
const crypto = require("crypto");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Item = require('./Item');
var ItemService = require('./ItemService');
var Location  = require('../location/Location');
var LocationService = require('../location/LocationService')
var UserService = require('../user/UserService')

var ItemImage = require('./ItemImage');
var User      = require('../user/User');

var Environment = require('../configs/environment');


const Storage = multer.diskStorage({
    destination(req, file, callback) {
      callback(null, './images')
    },
    filename(req, file, callback) {
      //callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`)
      callback(null, `${file.originalname}`)
    },
})
  
const upload = multer({ storage: Storage })

//this post will have images sent from emulator/phone
router.post('/', upload.array('image', 3), (req, res) => {
    (async () => {

        var location = await LocationService.GetLocation(req.body.address,
            req.body.street, req.body.city, req.body.postcode);

        var location2 = await LocationService.InsertLocation(req.body.address,
            req.body.street, req.body.city, req.body.postcode);

        //find user --s
        var user = await UserService.GetUser(req.body.email);

        var item = new Item({
            name: req.body.name,
            name: req.body.type,
            //location      : location[0],
            location: location2,
            userid: user.userid,
            description: req.body.description,
            dateAdded: req.body.dateAdded,
            isAvailable: req.body.isAvailable,
            dateAvailable: req.body.dateAvailable
        });

        console.log(req.files);

        item.save(
            function (err) {
                if (err) return res.status(500).send("There was a problem adding the information to the database.");

                var itemImages = [];
                const randomId = crypto.randomBytes(16).toString("hex");

                var generatedDir = randomId + "_" + item.id;
                var dirToSave = Environment.IMAGE_DIR + generatedDir;

                for (var key in req.files) {
                    let image1 = req.files[key];
                    if (!fs.existsSync(dirToSave)) {
                        fs.mkdirSync(dirToSave);
                    } 
                    var imgPath = dirToSave + '/' + image1.filename;
                    
                    fs.rename(image1.path, imgPath, function (err) {
                        if (err) throw err
                    });

                    var itemImage = new ItemImage({
                        description: req.body.imageDescription
                        ,imagePath: generatedDir + '/' + image1.filename
                        //,content: new Buffer(fs.readFileSync(imgPath)).toString("base64")
                    });

                    itemImage.save(function(err){});
                    itemImages.push(itemImage);                
                }                        

                item.itemImage = itemImages;
                item.save();
                res.status(200).send(item);
            });

    })()
});


// GETS  Itemm FROM THE DATABASE BY NAME
router.get('/name/:name', function (req, res) {
      
        try{
                ItemService.GetItemsByName(req.params.name, function (items) {
                if (!items) return res.status(404).send("No items found.");
                res.status(200).send(items);
            });   
        }
        catch(error)
        {
            console.log(error);
            return res.status(500).send("There was a problem finding the items.");
        }

});


// GETS  Itemm FROM THE DATABASE BY Postcode
router.get('/postcode/:postcode', function (req, res) {
      
    try{
            ItemService.GetItemsByPostcode(req.params.postcode, function (items) {
            if (!items) return res.status(404).send("No items found.");
            res.status(200).send(items);
        });
    }
    catch(error)
    {
        console.log(error);
        return res.status(500).send("There was a problem finding the items.");
    }

});

// GETS  Itemm FROM THE DATABASE BY Postcode
router.get('/suburb/:suburb', function (req, res) {
      
    try{
            ItemService.GetItemsBySuburb(req.params.suburb, function (items) {
            if (!items) return res.status(404).send("No items found.");
            res.status(200).send(items);
        });
    }
    catch(error)
    {
        console.log(error);
        return res.status(500).send("There was a problem finding the items.");
    }

});

//Get Items for either suburb or Name
router.get('/searchany/:any', function (req, res) {
      
    try{
            ItemService.GetItemsBySuburbOrName(req.params.any, function (items) {
            if (!items) return res.status(404).send("No items found.");
            res.status(200).send(items);
        });
    }
    catch(error)
    {
        console.log(error);
        return res.status(500).send("There was a problem finding the items.");
    }

});



// GETS  Itemm FROM THE DATABASE BY street and Postcode
router.get('/street/:street/postcode/:postcode', function (req, res) {
      
    try{
            ItemService.GetItemsByStreet(req.params.street, req.params.postcode, function (items) {
            if (!items) return res.status(404).send("No items found.");
            res.status(200).send(items);
        });
    }
    catch(error)
    {
        console.log(error);
        return res.status(500).send("There was a problem finding the items.");
    }

});

// GETS  Itemm FROM THE DATABASE BY User
router.get('/userid/:userid', function (req, res) {
    console.log("To return by userid " + req.params.userid); 
    try{
            ItemService.GetItemsByUser(req.params.userid, function (items) {
            if (!items) return res.status(404).send("No items found.");
            res.status(200).send(items);
        });
    }
    catch(error)
    {
        console.log(error);
        return res.status(500).send("There was a problem finding the items.");
    }

});

/*
router.get('/name/:name', function (req, res) {
    Item.find({'name': { $regex: '.*' + req.params.name + '.*' }  }, function (err, items) {
        if (err) return res.status(500).send("There was a problem finding the items.");
        if (!items) return res.status(404).send("No item found.");
        res.status(200).send(items);
    });
});
*/

// RETURNS ALL THE Items IN THE DATABASE
router.get('/', function (req, res) {
    Item.find({}).populate("itemImage").populate("location").exec(function (err, items) {
            if (err) return res.status(500).send("There was a problem finding the items.");
            res.status(200).send(items);
        });
    });
    

// GETS A SINGLE Item FROM THE DATABASE
router.get('/:id', function (req, res) {
    //console.log("To return " + req.params.id);
    ItemService.GetItemsByID(req.params.id, function (item, err) {        
        if (err) return res.status(500).send("There was a problem finding the Item.");
        if (!item) return res.status(404).send("No Item found.");
            res.status(200).send(item);
    });
 });

// DELETES A Item FROM THE DATABASE
router.delete('/:id', function (req, res) {
    console.log("To delete " + req.params.id);
    Item.findByIdAndRemove(req.params.id, function (err, item) {
        if (err) return res.status(500).send("There was a problem deleting the Item." + err);
        res.status(200).send("Item: "+ item.name +" was deleted.");
    });
});

// UPDATES A SINGLE Item IN THE DATABASE
router.put('/:id', function (req, res) {
    Item.findByIdAndUpdate(req.params.id, req.body, {new: true}, function (err, item) {
        if (err) return res.status(500).send("There was a problem updating the item.");
        res.status(200).send(item);
    });
});



module.exports = router;