var mongoose = require('mongoose');
var Environment = require('./configs/environment');

mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
var connection = mongoose.connect(Environment.DB_URL, { useNewUrlParser: true });



